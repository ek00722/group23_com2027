export default {
  data: [
    {
      icon: 'account_circle',
      label: 'User',
      color: 'blue',
      link: '/userinfo',
      separator: false
    },
    {
        icon: 'settings',
        label: 'Settings',
        color: 'blue-grey-7',
        link: '/settings',
        separator: true
      },
    {
        icon: 'help',
        label: 'Privacy',
        color: 'accent',
        link: '',
        separator: false
    },
    {
      icon: 'error',
      label: 'Report a Problem',
      color: 'red-6',
      link: '',
      separator: false
    },
    {
      icon: 'feedback',
      label: 'Send Feedback',
      color: 'orange',
      link: '',
      separator: true
    },
    {
    icon: 'logout',
    label: 'Logout',
    color: 'black',
    link: '',
    separator: false
    },
  ]
}